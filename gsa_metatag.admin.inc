<?php

/**
 * @file
 * Provides admin UI for the gsa_metatag module.
 */

/**
 * Google Appliance Metatags admin settings form.
 */
function gsa_metatag_settings_form() {
  $form = array();

  $form['#attached'] = array(
    'js' => array(
      drupal_get_path('module', 'gsa_metatag') . '/js/gsa_metatag.admin.js',
    ),
    'css' => array(
      drupal_get_path('module', 'gsa_metatag') . '/css/gsa_metatag.admin.css',
    ),
  );

  $form['notice'] = array(
    '#markup' => '<div class="messages warning">' . t('Entity body fields, and any textarea field are ommitted for all entity types, due to meta tags characters limitation.') . '</div>',
  );

  $form['entities'] = array(
    '#type' => 'fieldset',
    '#title' => t('Master controls for all entities'),
    '#description' => t("By enabling and disabling items here, it is possible to control which entities (e.g. nodes, taxonomy terms) and bundles (e.g. content types, vocabularies) will print GSA meta tags on their respective view pages. If an entity type is disabled it also disables it for <em>all</em> of that entity type's bundles."),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $defaults = gsa_metatag_get_default_selected_fields();
  foreach (entity_get_info() as $entity_type => $entity_info) {
    // Only show entities that are capable of using meta tags.
    if (gsa_metatag_entity_type_is_suitable($entity_type, $entity_info)) {
      $form['entities']['gsa_metatag_enable_' . $entity_type] = array(
        '#type' => 'checkbox',
        '#title' => t($entity_info['label']),
        '#default_value' => variable_get('gsa_metatag_enable_' . $entity_type, FALSE),
        '#attributes' => array(
          'class' => array('gsa-metatag-entity-checkbox'),
          'entity' => $entity_type,
        ),
      );

      $form[$entity_type . '_options'] = array(
        '#type' => 'fieldset',
        '#title' => t($entity_info['label'] . ' Metatag options'),
        '#description' => t('By enabling and disabling items here, it is possible to control which entity fields will print GSA meta tags on the entity view page.'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#states' => array(
          'visible' => array(
            ':input[name="gsa_metatag_enable_' . $entity_type . '"]' => array('checked' => TRUE),
          ),
        ),
      );

      // Some entities, e.g. User, (core) File, have a single bundle with the
      // same name as the entity, so only show the bundles list if there is
      // more than one of them and the bundle's name isn't the same as the
      // entity type's.
      if (!empty($entity_info['bundles'])) {
        foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
          if (count($entity_info['bundles']) > 1 || $entity_type != $bundle_name) {
            $form['entities']['gsa_metatag_enable_' . $entity_type . '__' . $bundle_name] = array(
              '#type' => 'checkbox',
              '#title' => t($bundle_info['label']),
              '#default_value' => variable_get('gsa_metatag_enable_' . $entity_type . '__' . $bundle_name, FALSE),
              '#attributes' => array(
                // Add some theming that'll indent this bundle.
                'class' => array('gsa-metatag-bundle-checkbox'),
                'entity' => $entity_type,
                'bundle' => $bundle_name,
              ),
              '#states' => array(
                'visible' => array(
                  ':input[name="gsa_metatag_enable_' . $entity_type . '"]' => array('checked' => TRUE),
                ),
              ),
            );

            $form[$entity_type . '_options'][$entity_type . '__' . $bundle_name . '_options'] = array(
              '#type' => 'fieldset',
              '#title' => t($bundle_info['label'] . ' Metatag options'),
              '#collapsible' => TRUE,
              '#collapsed' => FALSE,
              '#states' => array(
                'visible' => array(
                  ':input[name="gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '"]' => array('checked' => TRUE),
                ),
              ),
            );

            $fields = gsa_metatag_get_entity_fields($entity_type, $bundle_name);
            foreach ($fields as $field => $name) {
              if (variable_get('gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field, '') != '') {
                $default_value = variable_get('gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field, '');
              }
              else {
                $default_value = (isset($defaults[$entity_type][$field])) ? $defaults[$entity_type][$field] : FALSE;
              }

              $form[$entity_type . '_options'][$entity_type . '__' . $bundle_name . '_options']['gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field] = array(
                '#type' => 'checkbox',
                '#title' => t($name),
                '#default_value' => $default_value,
                '#disabled' => ($field == gsa_metatag_get_entity_identifier($entity_type)) ? TRUE : FALSE,
                '#attributes' => array(
                  'class' => array('gsa-metatag-bundle-field-checkbox'),
                  'entity' => $entity_type,
                  'bundle' => $bundle_name,
                ),
              );
            }
          }
          else {
            $fields = gsa_metatag_get_entity_fields($entity_type, $entity_type);
            foreach ($fields as $field => $name) {
              if (variable_get('gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field, '') != '') {
                $default_value = variable_get('gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field, '');
              }
              else {
                $default_value = (isset($defaults[$entity_type][$field])) ? $defaults[$entity_type][$field] : FALSE;
              }

              $form[$entity_type . '_options']['gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field] = array(
                '#type' => 'checkbox',
                '#title' => t($name),
                '#default_value' => $default_value,
                '#attributes' => array(
                  'class' => array('gsa-metatag-bundle-field-checkbox'),
                  'entity' => $entity_type,
                  'bundle' => $bundle_name,
                ),
              );
            }
          }
        }
      }
    }
  }

  $form['#validate'] = array('gsa_metatag_settings_form_validate');
  $form['#submit'] = array('gsa_metatag_settings_form_submit');

  return system_settings_form($form);
}

/**
 * Validation callback for GSA Metatags Settings form.
 */
function gsa_metatag_settings_form_validate(&$form, &$form_state) {
  $values = &$form_state['values'];

  foreach (entity_get_info() as $entity_type => $entity_info) {
    if (gsa_metatag_entity_type_is_suitable($entity_type, $entity_info)) {
      if (!empty($entity_info['bundles'])) {
        foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
          $fields = gsa_metatag_get_entity_fields($entity_type, $bundle_name);
          if (count($entity_info['bundles']) > 1 || $entity_type != $bundle_name) {
            foreach ($fields as $field => $name) {
              if (!$values['gsa_metatag_enable_' . $entity_type] && !$values['gsa_metatag_enable_' . $entity_type . '__' . $bundle_name]) {
                $values['gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field] = '';
              }
            }
          }
          else {
            foreach ($fields as $field => $name) {
              if (!$values['gsa_metatag_enable_' . $entity_type]) {
                $values['gsa_metatag_enable_' . $entity_type . '__' . $bundle_name . '__' . $field] = '';
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Submit callback for GSA Metatags Settings form.
 */
function gsa_metatag_settings_form_submit(&$form, &$form_state) {
  $vars = gsa_metatag_get_defined_vars();
  variable_set('gsa_metatag_defined_vars', $vars);
}
