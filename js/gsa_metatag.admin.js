/**
 * @file
 * Custom admin-side JS for the GSA Metatag module.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.gsaMetatagUISettingsPage = {
    attach: function (context) {
      $('.gsa-metatag-bundle-checkbox').click(function() {
        var type = $(this).attr('entity');
        var selected = $(this).closest('form').find('.gsa-metatag-bundle-checkbox[entity="' + type + '"]:checked').length;
        if (selected < 1) {
          $(this).closest('form').find('.gsa-metatag-entity-checkbox[entity="' + type + '"]').attr('checked', false);
        }
        else {
          $(this).closest('form').find('.gsa-metatag-entity-checkbox[entity="' + type + '"]').attr('checked', true);
        }
      });
    }
  };

})(jQuery);
